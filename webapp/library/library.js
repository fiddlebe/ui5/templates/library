sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/library'   // library dependency
], function(jQuery, library) {
    "use strict";
	
    /**
     *
     * @namespace 
     * @name ui5expresstemplate
     * @version 0.0.1
     * @public
     */

    // delegate further initialization of this library to the Core
    sap.ui.getCore().initLibrary({
        name : "ui5expresstemplate",
        version: "0.0.1",
        dependencies : [
            "sap.ui.core"
        ],
        types: [
            //watch out with types. they are not properly preloaded. 
            //consider defining your types under controls, if you want to access them via sap.ui.define
        ],
        interfaces: [],
        controls: [
            "ui5expresstemplate.xxxx"
        ],
        elements: [
        ],
        noLibraryCSS: true,
        i18n:"messagebundle.properties"        
    });   

    return ui5expresstemplate;

}, /* bExport= */ false);